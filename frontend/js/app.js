/*jshint strict:false */

var socket = io();
var app = angular.module('BlaJuke', ['jtt_youtube']);
var ipaddr = '';

function convert_time(duration) {
    var a = duration.match(/\d+/g);

    if (duration.indexOf('M') >= 0 && duration.indexOf('H') == -1 && duration.indexOf('S') == -1) {
        a = [0, a[0], 0];
    }

    if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1) {
        a = [a[0], 0, a[1]];
    }
    if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1 && duration.indexOf('S') == -1) {
        a = [a[0], 0, 0];
    }

    duration = 0;

    if (a.length == 3) {
        duration = duration + parseInt(a[0]) * 3600;
        duration = duration + parseInt(a[1]) * 60;
        duration = duration + parseInt(a[2]);
    }

    if (a.length == 2) {
        duration = duration + parseInt(a[0]) * 60;
        duration = duration + parseInt(a[1]);
    }

    if (a.length == 1) {
        duration = duration + parseInt(a[0]);
    }
    return duration
}

app.constant('jukeConfig', {
	"ytKey": "AIzaSyDxnvI_Wd3VAJ7HWqBjJoTW5iEdml9eVP8"
});

app.run(function ($rootScope) {
	$rootScope.data = {
		searchResult: [],
		playlist: [],
		currentVideo: {},
		history: []
	};
	socket.on('ipaddr', function (_ipaddr) {
		ipaddr = _ipaddr;
	});
	socket.on('playlist', function (playlist) {
		$rootScope.data.playlist = playlist;
		$rootScope.$apply();
	});
	socket.on('nextRandom', function (nextRandom) {
		$rootScope.data.nextRandom = nextRandom;
		$rootScope.$apply();
	});
	socket.on('currentVideo', function (currentVideo) {
		$rootScope.data.currentVideo = currentVideo;
		$rootScope.$apply();
	});
	socket.on('nextVideo', function (nextVideo) {
		$rootScope.data.history.unshift(nextVideo);
		$rootScope.$apply();
	});
	socket.on('userCount', function (userCount) {
		$rootScope.data.userCount = userCount;
		$rootScope.$apply();
	});

});

app.filter('voteStatus', function () {
	return function(video, status) {
		for(var ip in video.votes) {
			if(ip === ipaddr) {
				var vote = video.votes[ip];
				if(vote === status) {
					return true;
				}
			}
		}
	}
});

app.controller('MainController', function ($scope, $http, youtubeFactory, jukeConfig, $interval) {	
	$scope.searchVideos = function () {
		youtubeFactory.getVideoById({
			videoId: $scope.data.searchText,
			key: jukeConfig.ytKey,
			part: "contentDetails,id,snippet"
		}).then(function (res) {
			console.log(res.data.items);
			$scope.data.searchResult = res.data.items;
		});
	};
	
	$scope.addToQueue = function (video) {
		if(convert_time(video.contentDetails.duration) > 760) {
			alert('Lol, doe normaal');
		} else {
			socket.emit('addVideo', video);
		}
	};

	$scope.vote = function (videoId, newStatus) {
		socket.emit('vote', videoId, newStatus);
	};
	
});
