Promise = require 'bluebird'
express = require 'express'
app = express()
_http = require('http')
http = _http.Server(app)
_http = require('follow-redirects').http
io = require('socket.io')(http)
mongoose = require 'mongoose'

currentVideo = {}
nextRandom = {}
userCount = 0

sortVotes = (a,b) ->
    if a.voteCount < b.voteCount
        1
    else if a.voteCount > b.voteCount
        -1
    else
        0

mongoose.connect 'mongodb://localhost/blajuke'

VideoSchema = new mongoose.Schema(
    videoId: String,
    title: String,
    thumbnail: String,
    votes: Object
, { toObject: { virtuals: true }, toJSON: {virtuals:true}})

VideoSchema.virtual("voteCount").get () ->
    count = 0
    for ip,vote of this.votes
        if vote
            count++
        else
            count--
    count

VideoSchema.statics.random = (callback) ->
  @count ((err, count) ->
    if err
      return callback(err)
    rand = Math.floor(Math.random() * count)
    @findOne().skip(rand).exec callback
    return
  ).bind(this)
  return

Video = mongoose.model 'videos', VideoSchema
ArchivedVideo = mongoose.model 'videos_archived', VideoSchema

app.use express.static 'frontend'

setRandom = ->
    ArchivedVideo.random (err, archived) ->
        if archived?
            nextRandom = archived
            io.emit 'nextRandom', nextRandom

setRandom()

sendPlaylist = ->
    Video.find {}, (err, videos) ->
        videos.sort sortVotes
        io.emit 'playlist', videos
        io.emit 'currentVideo', currentVideo
        io.emit 'nextRandom', nextRandom

io.on 'connection', (socket) ->
    socket.data = {}
    userCount++
    io.emit 'userCount', userCount
    sendPlaylist()

    socket.ipaddr = socket.handshake.headers['x-forwarded-for'] || socket.handshake.address
    socket.ipaddr = socket.ipaddr.replace(/\W/gi, '_')
    socket.emit 'ipaddr', socket.ipaddr

    socket.on 'disconnect', () ->
        userCount--
        io.emit 'userCount', userCount

    socket.on 'addVideo', (video) ->
        ip = socket.ipaddr
        id = video.id
        newVideo = 
            videoId: id
            title: video.snippet.title,
            thumbnail: video.snippet.thumbnails.default.url,
            votes: {
                "#{ip}": true
            }
        Video.findOne { videoId: id }, (err, video) ->
            if video
                video.votes[ip] = true
                video.markModified('votes')
                video.save((err, res) ->
                    sendPlaylist()
                )
            else
                new Video(newVideo).save (error) ->
                   sendPlaylist()
                   console.log error
    
    socket.on 'vote', (videoId, newStatus) ->
        ip = socket.ipaddr 
        Video.findOne { videoId: videoId }, (err, video) ->
            if video
                video.votes[ip] = newStatus
                video.markModified('votes')
                video.save (err, res) ->
                    sendPlaylist()
    
    socket.on 'requestNext', () ->
        Video.find {}, (err, videos) ->
            videos.sort sortVotes
            random = Math.floor(Math.random() * 2)
            if videos.length > 0 and (videos[0].voteCount > 0 or random is 1)
                setTimeout ->
                    io.emit 'nextVideo', videos[0]
                    currentVideo = videos[0].toObject()
                    currentVideo.startTime = new Date().getTime() / 1000
                    
                    if currentVideo.voteCount > 0
                        ArchivedVideo.find {videoId: currentVideo.videoId}, (err, archivedvids) ->
                            if archivedvids.length is 0
                                archiveVideo = 
                                    videoId: currentVideo.videoId
                                    title: currentVideo.title
                                    thumbnail: currentVideo.thumbnail
                                    votes: currentVideo.votes
                                new ArchivedVideo(archiveVideo).save()
                    videos[0].remove (err) ->
                        sendPlaylist()
                , 1000
            else
                if nextRandom.length is 0
                    setTimeout ->
                        io.emit 'nextVideo', videoId: 'nofound'
                    , 1000
                else
                    setTimeout ->
                        currentVideo = nextRandom.toObject()
                        currentVideo.startTime = new Date().getTime() / 1000
                        io.emit 'nextVideo', nextRandom
                        setRandom()
                        sendPlaylist()
                    , 1000
    
    socket.on 'requestArchive', () ->
        ArchivedVideo.find {}, (err, videos) ->
            socket.emit 'archive', videos
    
    socket.on 'removeArchive', (id) ->
        ArchivedVideo.remove {videoId: id}, (err, videos) ->
            ArchivedVideo.find {}, (err, videos) ->
                socket.emit 'archive', videos

http.listen(1337)
